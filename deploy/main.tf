terraform {
  backend "s3" {
    bucket         = "dhiman-recipe-app-terraform-state"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    dynamodb_table = "dhiman-recipe-app-terraform-statelock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}