variable "prefix" {
  default = "raad"
}

variable "project" {
  type        = string
  default     = "recipe-app-api-devops"
  description = "Name of the project"
}

variable "contact" {
  type        = string
  default     = "dhiman.poison@gmail.com"
  description = "owner of the resources"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}
